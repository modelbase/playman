<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracks extends Model
{
    protected $fillable = ['artist_name','track_name','playlist_id','image','plays','duration','likes','user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
