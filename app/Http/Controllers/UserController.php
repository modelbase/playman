<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Response;

class UserController extends Controller
{
    /**
     * checks if user is logged in
     *
     * @return boolean
     */
    public function isUserLoggedIn()
    {
        if (Auth::user()) {
            return ['user'=> Auth::user()];
        } else {
            return Response::json(array(
                'code'      =>  401,
                'message'   =>  'User Not Logged In'
            ), 401);
        }
    }
}
