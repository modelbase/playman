<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tracks;
use Auth;

class TracksController extends Controller
{
    /**
     * store the tracks
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->middleware('auth');

        $validatedData = $request->validate([
            'artist_name' => 'required',
            'track_name' => 'required',
            'playlist_id' => 'required',
        ]);

        if (Tracks::where(
            [
                'artist_name'=> $request->input('artist_name'),
                'track_name'=> $request->input('track_name')
            ]
        )->first()) {
            return ['error'=>'Already in playlist.'];
        }

        $track = Tracks::create($request->all());
        $track->user_id = Auth::user()->id;
        $track->save();

        return $track;
    }


    /**
     * get all the tracks
     *
     * @return void
     */
    public function getAllTracks()
    {
        $tracks = Tracks::with('user')->orderBy('likes', 'desc')->orderBy('created_at', 'desc')->orderBy('artist_name', 'asc')->get();
        
        return $tracks;
    }


    /**
     * stores likes
     *
     * @param Request $request
     * @return void
     */
    public function addTrackLikes(Request $request)
    {
        $this->middleware('auth');

        $track_id = $request->input('track_id');

        $track = Tracks::find($track_id);

        if ($track) {
            $likes = $track->likes;
            $track->likes = $likes + 1;
            $track->save();
        }

        return $track->likes;
    }

    public function addTrackLikes2()
    {
        $tracks = Tracks::with('user')->get();
        
        dd($tracks) ;
    }

    public function dellall()
    {
        $this->middleware('auth');

        $tracks = Tracks::all();
        foreach ($tracks as $t) {
            $t->delete();
        }
        
        dd('done');
    }
}
