<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use \Hash;
use App\Tracks;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tracks = Tracks::all();

        return view('home', compact('tracks', 'playlist'));
    }

    public function bitch()
    {
        $user = new User();
        $user->password = Hash::make('admin');
        $user->email = 'admin@admin.com';
        $user->name = 'playmaster';
        //$user->save();
    }
}
