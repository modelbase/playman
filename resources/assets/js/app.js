/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import SuiVue from 'semantic-ui-vue';



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('track-list', require('./components/TrackList.vue'));
Vue.component('search-tracks', require('./components/SearchTracks.vue'));

Vue.use(SuiVue);

window.Event = new Vue();

const app = new Vue({

    el: '#mainapp',

    data() {
        return {
        };
    },

    mounted() {
        this.isUserLoggedIn();
    },


    created() {

    },

    methods: {

        isUserLoggedIn() {
            axios.get('/user')
                .then(function (response) {
                    //console.log(response);
                })
                .catch(function (error) {
                    //console.log(error);
                });
        }
    }
});