@extends('layouts.app')

@section('content')

<div class="ui grid">

        <div class="ui grid">
                <div class=" wide column">
                    <div class="ui segment">
                            <div class="ui icon message floating">
                                    <i class="music icon"></i>
                                    <div class="content">
                                      <div class="header">
                                        Open Playlist Manager:
                                      </div>
                                    </div>
                                  </div>
                        @include('includes.search.search_form')
                    </div>
        
                   
                        @include('includes.play_list')

                </div>
            </div>
        </div>

@endsection
