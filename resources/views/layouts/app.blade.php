
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<html>
@include('includes.head')

  <body>

    <div id="mainapp">

    @include('includes.nav')
  
      <div id="">
       <div class="ui container">
          @yield('content')
        </div>
      </div>

      

    <!-- /.container -->

      @include('includes.footer')

      @include('includes.scripts')
        
      @yield('scripts')



    </div>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
  <script src="{{ asset('/js/app.js') }}"></script>



</html>
