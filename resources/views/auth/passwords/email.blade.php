@extends('layouts.app')

@section('content')




<div class="ui middle aligned center aligned grid">
    <div class="column" style="width:50%">
      <h2 class="ui teal image header">
        <div class="content">
          Password Reset
        </div>
      </h2>
      @if (session('status'))
      <div class="ui segment">
          {{ session('status') }}
      </div>
  @endif


                    <form method="POST" action="{{ route('password.email') }}" class="ui large form">
                        @csrf
        <div class="ui stacked segment">
          <div class="field">
            <div class="ui left icon input">
              <i class="mail icon"></i>
              <input id="email" placeholder="Email Address" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

              @if ($errors->has('email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <button type="submit" class="ui fluid large teal submit button">  {{ __('Send Password Reset Link') }}</button>
        </div>
  
        <div class="ui error message"> asd asda asd asd asd</div>
  
      </form>
  
    </div>
  </div>



@endsection
