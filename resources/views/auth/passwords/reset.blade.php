@extends('layouts.app')

@section('content')


<div class="ui middle aligned center aligned grid">
    <div class="column" style="width:50%">
      <h2 class="ui teal image header">
        <div class="content">
            {{ __('Reset Password') }}
        </div>
      </h2>


                        <form method="POST" action="{{ route('password.request') }}" class="ui large form">
                            @csrf
    
                            <input type="hidden" name="token" value="{{ $token }}">
        <div class="ui stacked segment">


          <div class="field">
            <div class="ui left icon input">
              <i class="user icon"></i>


          <input id="email" type="text" placeholder="Email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

          @if ($errors->has('email'))
              <span class="invalid-feedback">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif

            </div>
          </div>
          <div class="field">
            <div class="ui left icon input">
              <i class="lock icon"></i>

              <input id="password" type="password" placeholder="Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

              @if ($errors->has('password'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          <div class="field">
            <div class="ui left icon input">
              <i class="lock icon"></i>


              <input id="password_confirm" type="password" placeholder="Password Confirm"  class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

              @if ($errors->has('password_confirmation'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          
          <button type="submit" class="ui fluid large teal submit button"> {{ __('Reset Password') }}</button>
        </div>
  
        <div class="ui error message"> </div>
  
      </form>
  
    </div>
  </div>
  

@endsection
