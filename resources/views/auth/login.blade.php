@extends('layouts.app')

@section('content')

<div class="ui middle aligned center aligned grid">
        <div class="column" style="width:50%">
          <h2 class="ui teal image header">
            <div class="content">
              Log-in to Playlist Manager
            </div>
          </h2>

                <form method="POST" action="{{ route('login') }}" class="ui large form">
                        @csrf
            <div class="ui stacked segment">
              <div class="field">
                <div class="ui left icon input">
                  <i class="mail icon"></i>
                  <input  type="text" placeholder="E-mail address" class="form-control{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                  @if ($errors->has('email'))
                  <small class="ui text invalid-feedback error">
                      <strong>{{ $errors->first('email') }}</strong>
                  </small>
              @endif
                </div>
              </div>
              <div class="field">
                <div class="ui left icon input">
                  <i class="lock icon"></i>

                  <input id="password" type="password" placeholder="Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                  @if ($errors->has('password'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="field left align">
       

                <div class="ui checkbox ">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                        <label>{{ __('Remember Me') }}</label>
                      </div>
              </div>

              <button type="submit" class="ui fluid large teal submit button">Login</button>
            </div>
      
            <div class="ui error message"> asd asda asd asd asd</div>
      
          </form>
      
          <div class="ui message">

            <a class="ui button" href="{{ route('register') }}">{{ __('Register') }}</a>
            
            <a class="ui button" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
          </div>
        </div>
      </div>
      





@endsection
