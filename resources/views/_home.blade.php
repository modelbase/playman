

@extends('layouts.main')

@section('main')

    {{--  <div class="ui grid">
        <div class="column padding-reset">
            <div class="ui huge message page grid">
                <h1 class="ui huge header">Hello, world!</h1>
                <p>This is a snippet for any type of website. It includes a call-to-action button and looks kind of like the jumbotron from Bootstrap. It does not require any additional CSS rules.</p>
                <a class="ui blue button">Learn more »</a>
            </div>
        </div>
    </div>  --}}

    <div class="ui grid">
        <div class="eleven wide column">
            <div class="ui segment">
                @include('includes.search.search_form')
            </div>

            <div class="ui segment">
                @include('includes.play_list')
            </div>
        </div>
        <div class="five wide column">
            <div class="ui segment">
                @include('includes.sidebar')
            </div>
        </div>
    </div>

@endsection

