

<div class="ui inverted vertical footer segment footer-section" style="margin-top:40px;">
  <div class="ui center aligned container">

      <div class="ten wide column">
        <h4 class="ui inverted header">Open Playlist</h4>
        <p>Search for your track then add it to the playlist.</p>
      </div>

  </div>
</div>