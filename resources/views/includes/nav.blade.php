
    <div class="ui fixed inverted borderless menu">
            <div class="ui container">
              <a class="header item" href="/">{{ Config::get('app.name', 'Open Playlist') }}</a>
              <a class="active item" href="/">Home</a>

              <div class="right menu">
                  @guest
                  <a class="item" href="{{ route('login') }}">{{ __('Login') }}</a>
                <a class="item" href="{{ route('register') }}">{{ __('Register') }}</a>
              @else
                <a class="item" href="{{ route('logout') }}">{{ __('Logout') }}</a>
                @endguest
              </div>
            </div>
          </div>


 

  
  