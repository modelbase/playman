<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/bitch', 'HomeController@bitch');
Route::get('/', 'HomeController@index');


Route::post('/tracks/store', 'TracksController@store');
Route::post('/tracks/likes/add', 'TracksController@addTrackLikes');
Route::get('/tracks/likes/key', 'TracksController@addTrackLikes2');
Route::get('/tracks/likes/del', 'TracksController@dellall');

Route::get('/tracks/all', 'TracksController@getAllTracks');


Route::get('/user', 'UserController@isUserLoggedIn');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
