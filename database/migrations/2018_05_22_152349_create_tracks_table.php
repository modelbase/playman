<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('artist_name')->nullable();
            $table->string('track_name')->nullable();
            $table->string('image')->nullable();
            $table->string('type')->nullable();
            $table->string('plays')->nullable();
            $table->string('duration')->nullable();
            $table->integer('likes')->nullable();
            $table->integer('playlist_id')->nullable();
            $table->integer('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
    }
}
